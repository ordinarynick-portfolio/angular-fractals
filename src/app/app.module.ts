import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FractalInfoComponent } from './components/fractal-info/fractal-info.component';
import { FractalSettingsComponent } from './components/fractal-settings/fractal-settings.component';
import { FractalControlsComponent } from './components/fractal-controls/fractal-controls.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AboutComponent } from './components/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FractalInfoComponent,
    FractalSettingsComponent,
    FractalControlsComponent,
    SidebarComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
