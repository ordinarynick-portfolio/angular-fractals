/**
 * Settings for {@link IFractal}.
 */
export interface IFractalSettings {

  /**
   * Count iterations for {@link IFractal} computing/rendering.
   */
  iterations: number;
  /**
   * Width of result image from {@link IFractal}.
   */
  width: number;
  /**
   * Height of result image from {@link IFractal}.
   */
  height: number;
  /**
   * X position of view on {@link IFractal}.
   */
  xOffset: number;
  /**
   * Y position of view on {@link IFractal}.
   */
  yOffset: number;
  /**
   * Zoom of view on {@link IFractal}.
   */
  zoom: number;

}
