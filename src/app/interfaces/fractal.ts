import {IFractalSettings} from './fractal-settings';

/**
 * Interface for defining strategy for drawing fractal.
 */
export interface IFractal<Settings extends IFractalSettings> {

  /**
   * Render {@link IFractal} to {@link HTMLCanvasElement} for given {@link IFractalSettings}.
   *
   * @param settings {@link IFractalSettings} for {@link IFractal}.
   * @param canvas {@link HTMLCanvasElement} on which will be {@link IFractal} rendered.
   */
  render(canvas: HTMLCanvasElement, settings: Settings): void;

  /**
   * Creates {@link IFractalSettings} for this {@link IFractal} with default values.
   *
   * @return Instance of {@link IFractalSettings}.
   */
  createSettings(): Settings;
}
