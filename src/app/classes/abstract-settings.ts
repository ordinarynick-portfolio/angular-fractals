import {IFractalSettings} from '../interfaces/fractal-settings';

export class AbstractSettings implements IFractalSettings {

  iterations: number;
  width: number;
  height: number;
  xOffset: number;
  yOffset: number;
  zoom: number;

  constructor(iterations: number, width: number, height: number, xOffset: number, yOffset: number, zoom: number) {
    this.iterations = iterations;
    this.width = width;
    this.height = height;
    this.xOffset = xOffset;
    this.yOffset = yOffset;
    this.zoom = zoom;
  }
}
