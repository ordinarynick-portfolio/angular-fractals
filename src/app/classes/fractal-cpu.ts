import {IFractal} from '../interfaces/fractal';
import {IFractalSettings} from '../interfaces/fractal-settings';
import {Color} from './data/color';

/**
 * Base class for {@link IFractal} which are computed by CPU.
 */
export abstract class FractalCpu<Settings extends IFractalSettings> implements IFractal<Settings> {

  abstract createSettings(): Settings;

  render(canvas: HTMLCanvasElement, settings: Settings): void {
    const context2D = canvas.getContext('2d');
    const canvasData = context2D.createImageData(settings.width, settings.height);

    for (let x = 0; x < settings.width; x++) {
      for (let y = 0; y < settings.height; y++) {
        const color = this.computePoint(settings, x, y);
        this.drawPixel(canvasData, settings.width, x, y, color);
      }
    }

    context2D.putImageData(canvasData, 0, 0);
  }

  /**
   * Compute one point of {@link IFractal}.
   *
   * @param settings {@link IFractalSettings} for computing point of {@link IFractal}.
   * @param x Position of view on {@link IFractal}.
   * @param y Position of view on {@link IFractal}.
   *
   * @return {@link Color} for given point.
   */
  protected abstract computePoint(settings: Settings, x: number, y: number): Color;

  /**
   * Draw pixel on {@link ImageData}.
   *
   * @param canvasData {@link ImageData} which will be edited.
   * @param width Width of canvas, to get right index.
   * @param x Position x.
   * @param y Position y.
   * @param color {@link Color} which will be used to draw pixel.
   */
  private drawPixel(canvasData: ImageData, width: number, x: number, y: number, color: Color) {
    const index = (x + (y * width)) * 4;

    canvasData.data[index] = color.red;
    canvasData.data[index + 1] = color.green;
    canvasData.data[index + 2] = color.blue;
    canvasData.data[index + 3] = color.alpha;
  }
}
