import {Color} from './color';

/**
 * Simple immutable class which represent one pixel.
 */
export class Pixel {

  readonly x: number;
  readonly y: number;
  readonly color: Color;

  constructor(x: number, y: number, color: Color) {
    this.x = x;
    this.y = y;
    this.color = color;
  }
}
