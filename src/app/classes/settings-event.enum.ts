import {SettingsType} from '../enums/settings-type.enum';

export class SettingsEvent {

  private readonly _type: SettingsType;
  private readonly _value: number;

  constructor(type: SettingsType, value: number) {
    this._type = type;
    this._value = value;
  }

  get type(): SettingsType {
    return this._type;
  }

  get value(): number {
    return this._value;
  }
}
