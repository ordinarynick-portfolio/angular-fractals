import {IFractal} from '../../interfaces/fractal';
import {MandelbrotSettings} from './mandelbrot-settings';
import * as gpu from 'gpu.js';
import {IFractalSettings} from '../../interfaces/fractal-settings';

// FIXME this class.
export class MandelbrotGpu implements IFractal<MandelbrotSettings> {

  settings: MandelbrotSettings;
  kernel;

  constructor(width, height) {
    this.settings = new MandelbrotSettings();
    // FIXME Vytvořit kernel tak, aby obsahoval jednotlivé funkce pro complexní čísla a pak se tyto funkce (kernely postupně volali)
    this.kernel = new gpu().createKernel(function (iterations, xOffset, yOffset, zoom) {
      const x = (this.thread.x - xOffset) / zoom;
      const y = (this.thread.y - yOffset) / zoom;
      const colorIterValue = 255 / iterations;

      let z = 0.0;
      let zi = 0.0;
      let color = 0;

      for (let i = 0; i < iterations; i++) {
        const newZ = z * z - zi * zi + x;
        const newZi = 2 * z * zi + y;

        const absValue = Math.sqrt(newZ * newZ + newZi * newZi);

        if (absValue > 2) {
          color = colorIterValue * i;
          break;
        }

        z = newZ;
        zi = newZi;
      }

      this.color(color, color, color, 255);
    }, {
      dimensions: [width, height],
      output: [width, height],
      graphical: true,
      loopMaxIterations: 100000
    });
    /*    this.kernel = new gpu().createKernel(function (iterations, xOffset, yOffset, zoom) {
          const x = (this.thread.x - xOffset) / zoom;
          const y = (this.thread.y - yOffset) / zoom;
          const colorIterValue = 255 / iterations;
          const MINDIFF = 0.1;

          let z = 0.0;
          let zi = 0.0;
          let color = 0;

          for (let i = 0; i < iterations; i++) {
            const newZ = z * z - zi * zi + x;
            const newZi = 2 * z * zi + y;
            // Here is only, because of bad prod compiling
            z = newZ;
            const sum = newZ * newZ + newZi * newZi;

            //////////////////////////////////////////////////////////////////////////////////
            // Custom sqrt function
            let root = sum;
            let diff = 1;
            // Here is only, because of bad prod compiling
            zi = newZi;
            do {
              root = (root + diff) / 2;
              diff = sum / root;
            } while (root - diff > MINDIFF);
            //////////////////////////////////////////////////////////////////////////////////

            if (root > 2) {
              color = colorIterValue * i;
              //color = 255;
              this.color(color, color, color, 255);
              //this.color(0, 0, 0, 255);
              //this.color(255, 255, 255, 255);
              break;
            }
          }

          //this.color(color, color, color, 255);
        }, {
          dimensions: [width, height],
          output: [width, height],
          graphical: true,
          loopMaxIterations: 100000,
        });*/
  }

  render(canvas: HTMLCanvasElement, settings: MandelbrotSettings): void {
    //this.kernel.dimensions = [settings.width, settings.height];
    this.kernel(settings.iterations, settings.xOffset, settings.yOffset, settings.zoom);
  }

  getCanvas(): Node {
    // FIXME Think how to do it better!
    return this.kernel.getCanvas();
  }

  getSettings(): IFractalSettings {
    return this.settings;
  }

  createSettings(): MandelbrotSettings {
    return undefined;
  }
}
