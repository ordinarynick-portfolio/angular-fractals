import {MandelbrotSettings} from './mandelbrot-settings';
import {FractalCpu} from '../fractal-cpu';
import {Color} from '../data/color';

/**
 * Mandelbrot fractal computed on CPU.
 */
export class MandelbrotCpu extends FractalCpu<MandelbrotSettings> {

  constructor() {
    super();
  }

  createSettings(): MandelbrotSettings {
    return new MandelbrotSettings();
  }

  protected computePoint(settings: MandelbrotSettings, px: number, py: number): Color {
    const x = (px - settings.xOffset) / settings.zoom;
    const y = (py - settings.yOffset) / settings.zoom;
    const colorIterValue = 255 / settings.iterations;

    let z = 0.0;
    let zi = 0.0;
    let color = 0;

    for (let i = 0; i < settings.iterations; i++) {
      const newZ = z * z - zi * zi + x;
      const newZi = 2 * z * zi + y;

      const absValue = Math.sqrt(newZ * newZ + newZi * newZi);

      if (absValue > 2) {
        color = colorIterValue * i;
        break;
      }

      z = newZ;
      zi = newZi;
    }

    return new Color(color, color, color, 255);
  }
}
