export enum SettingsType {
  DIRECTION_X,
  DIRECTION_Y,
  ITERATIONS,
  ZOOM,
  RESET
}
