import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FractalSettingsComponent } from './fractal-settings.component';

describe('FractalSettingsComponent', () => {
  let component: FractalSettingsComponent;
  let fixture: ComponentFixture<FractalSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FractalSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FractalSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
