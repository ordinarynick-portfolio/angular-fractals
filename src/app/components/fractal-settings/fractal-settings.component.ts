import {Component, OnInit} from '@angular/core';
import {InteractionService} from '../../services/interaction.service';
import {FractalService} from '../../services/fractal.service';
import {IFractalSettings} from '../../interfaces/fractal-settings';

@Component({
  selector: 'app-fractal-settings',
  templateUrl: './fractal-settings.component.html',
  styleUrls: ['./fractal-settings.component.css']
})
export class FractalSettingsComponent implements OnInit {

  visible: boolean;
  settings: IFractalSettings;

  constructor(
    private interactionService: InteractionService,
    public fractalService: FractalService) {
  }

  ngOnInit() {
    const visibility = this.interactionService.fractalSettingsVisibility;
    const settingsObservable = this.fractalService.getFractalSettingsObservable();

    visibility.subscribe((visible) => this.visible = visible);
    settingsObservable.subscribe((settings) => this.settings = settings);
  }
}
