import {Component, OnInit} from '@angular/core';
import {InteractionService} from '../../services/interaction.service';
import {FractalService} from '../../services/fractal.service';
import {IFractalSettings} from '../../interfaces/fractal-settings';

@Component({
  selector: 'app-fractal-info',
  templateUrl: './fractal-info.component.html',
  styleUrls: ['./fractal-info.component.css']
})
export class FractalInfoComponent implements OnInit {

  visible: boolean;
  settings: IFractalSettings;

  constructor(
    private interactionService: InteractionService,
    public fractalService: FractalService
  ) {
  }

  ngOnInit() {
    const visibility = this.interactionService.fractalInfoVisibility;
    const settingsObservable = this.fractalService.getFractalSettingsObservable();


    visibility.subscribe((visible) => this.visible = visible);
    settingsObservable.subscribe((settings) => this.settings = settings);
  }
}
