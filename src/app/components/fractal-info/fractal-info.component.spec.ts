import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FractalInfoComponent } from './fractal-info.component';

describe('FractalInfoComponent', () => {
  let component: FractalInfoComponent;
  let fixture: ComponentFixture<FractalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FractalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FractalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
