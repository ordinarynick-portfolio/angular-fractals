import {Component, OnInit} from '@angular/core';
import {InteractionService} from '../../services/interaction.service';

@Component({
  selector: 'app-fractal-controls',
  templateUrl: './fractal-controls.component.html',
  styleUrls: ['./fractal-controls.component.css']
})
export class FractalControlsComponent implements OnInit {

  visible: boolean;

  constructor(
    private interactionService: InteractionService,
  ) {
  }

  ngOnInit() {
    const visibility = this.interactionService.fractalControlsVisibility;

    visibility.subscribe((visible) => this.visible = visible);
  }
}
