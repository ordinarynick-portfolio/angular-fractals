import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FractalControlsComponent } from './fractal-controls.component';

describe('FractalControlsComponent', () => {
  let component: FractalControlsComponent;
  let fixture: ComponentFixture<FractalControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FractalControlsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FractalControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
