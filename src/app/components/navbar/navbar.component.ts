import {Component, OnInit} from '@angular/core';
import {InteractionService} from '../../services/interaction.service';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  private settingsVisibility: Subject<boolean>;
  private controlsVisibility: Subject<boolean>;
  private infoVisibility: Subject<boolean>;

  public settingsVisible: boolean;
  public controlsVisible: boolean;
  public infoVisible: boolean;

  constructor(
    private interactionService: InteractionService
  ) {
  }

  ngOnInit() {
    this.settingsVisibility = this.interactionService.fractalSettingsVisibility;
    this.controlsVisibility = this.interactionService.fractalControlsVisibility;
    this.infoVisibility = this.interactionService.fractalInfoVisibility;

    this.settingsVisibility.subscribe((visible) => this.settingsVisible = visible);
    this.controlsVisibility.subscribe((visible) => this.controlsVisible = visible);
    this.infoVisibility.subscribe((visible) => this.infoVisible = visible);
  }

  switchFractalInfoVisibility(): void {
    this.infoVisibility.next(!this.infoVisible);
  }

  switchFractalSettingsVisibility(): void {
    this.settingsVisibility.next(!this.settingsVisible);
  }

  switchFractalControlsVisibility(): void {
    this.controlsVisibility.next(!this.controlsVisible);
  }

  exportCanvasToImage(): void {
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    const image = canvas
      .toDataURL('image/png')
      .replace('image/png', 'image/octet-stream');

    const link = document.createElement('a');
    link.download = 'fractal.png';
    link.href = image;
    link.click();
  }
}
