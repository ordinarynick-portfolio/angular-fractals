import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SettingsEvent} from '../classes/settings-event.enum';
import {SettingsType} from '../enums/settings-type.enum';

/**
 * Simple service to catch {@link KeyboardEvent} and translate them to {@link SettingsEvent}.
 */
@Injectable({
  providedIn: 'root'
})
export class KeyboardService {

  // Movement codes
  private readonly arrowUpCode = 38;
  private readonly arrowDownCode = 40;
  private readonly arrowLeftCode = 37;
  private readonly arrowRightCode = 39;
  private readonly movementKeys = new Set([this.arrowUpCode, this.arrowDownCode, this.arrowLeftCode, this.arrowRightCode]);

  // Zoom codes
  private readonly plusCode = 107;
  private readonly minusCode = 189;
  private readonly zoomKeys = new Set([this.plusCode, this.minusCode]);

  // Other codes
  private readonly iterationsCode = 73;

  private readonly settingsEventStream: BehaviorSubject<SettingsEvent> =
    new BehaviorSubject<SettingsEvent>(new SettingsEvent(SettingsType.RESET, 0));

  constructor() {
    this.registerEventListeners();
  }

  /**
   * @return Returns {@link Observable} of {@link SettingsEvent}s.
   */
  public getSettingEvents(): Observable<SettingsEvent> {
    return this.settingsEventStream;
  }

  private registerEventListeners() {
    window.addEventListener('keydown', (event) => this.handleKeyboardEvent(event));
  }

  private handleKeyboardEvent(event: KeyboardEvent) {
    let baseValue;

    // FIXME Refactor keyCode to code
    if (this.movementKeys.has(event.keyCode)) {
      baseValue = 10;
    } else if (this.zoomKeys.has(event.keyCode)) {
      baseValue = 25;
    } else if (this.iterationsCode === event.keyCode) {
      baseValue = 100;
    }

    const value = baseValue * (event.ctrlKey ? 10 : 1) * (event.altKey ? 100 : 1) * (event.shiftKey ? -1 : 1);

    let settingsEvent;
    switch (event.keyCode) {
      case this.arrowUpCode:
        settingsEvent = new SettingsEvent(SettingsType.DIRECTION_Y, -value);
        break;
      case this.arrowDownCode:
        settingsEvent = new SettingsEvent(SettingsType.DIRECTION_Y, value);
        break;
      case this.arrowLeftCode:
        settingsEvent = new SettingsEvent(SettingsType.DIRECTION_X, -value);
        break;
      case this.arrowRightCode:
        settingsEvent = new SettingsEvent(SettingsType.DIRECTION_X, value);
        break;
      case this.plusCode:
        settingsEvent = new SettingsEvent(SettingsType.ZOOM, value);
        break;
      case this.minusCode:
        settingsEvent = new SettingsEvent(SettingsType.ZOOM, -value);
        break;
      case this.iterationsCode:
        settingsEvent = new SettingsEvent(SettingsType.ITERATIONS, value);
        break;
    }

    if (settingsEvent) {
      this.settingsEventStream.next(settingsEvent);
    }
  }
}
