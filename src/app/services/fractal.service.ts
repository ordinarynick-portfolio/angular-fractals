import {Injectable} from '@angular/core';
import {IFractal} from '../interfaces/fractal';
import {IFractalSettings} from '../interfaces/fractal-settings';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {MandelbrotCpu} from '../classes/mandelbrot/mandelbrotCpu';
import {InteractionService} from './interaction.service';
import {SettingsEvent} from '../classes/settings-event.enum';
import {SettingsType} from '../enums/settings-type.enum';

@Injectable({
  providedIn: 'root'
})
/**
 * Service which holds information about currently selected {@link IFractal} and their information.
 */
export class FractalService {

  /**
   * {@link Observable} of {@link IFractal}s, which represents changes of {@link IFractal}.
   */
  private readonly fractalSubject: Subject<IFractal<IFractalSettings>> = new BehaviorSubject(null);
  /**
   * {@link Observable} of {@link IFractalSettings}s, which represents changes of {@link IFractalSettings}.
   */
  private readonly settingsSubject: BehaviorSubject<IFractalSettings> = new BehaviorSubject(null);

  constructor(
    private interactionService: InteractionService,
  ) {
    this.setFractal(new MandelbrotCpu());

    this.registerSubscribers();
  }

  /**
   * @return {@link Observable} of {@link IFractal} changes.
   */
  public getFractalObservable(): Observable<IFractal<IFractalSettings>> {
    return this.fractalSubject;
  }

  /**
   * @return {@link Observable} of {@link IFractalSettings} changes.
   */
  public getFractalSettingsObservable(): Observable<IFractalSettings> {
    return this.settingsSubject;
  }

  private setFractal(fractal: IFractal<IFractalSettings>): void {
    this.fractalSubject.next(fractal);
  }

  private registerSubscribers(): void {
    this.fractalSubject.subscribe(fractal => {
      const settings = fractal.createSettings();

      this.setDimensions(settings);
      this.resetPosition(settings);

      this.settingsSubject.next(settings);
    });
    this.interactionService.settingsEventStream.subscribe((event) => {
      this.applySettingsChange(event);
    });
  }

  // FIXME Maybe create fractal settings service? :D
  private setDimensions(settings: IFractalSettings): void {
    settings.width = innerWidth;
    settings.height = innerHeight;
  }

  private resetPosition(settings: IFractalSettings): void {
    settings.xOffset = settings.width / 2;
    settings.yOffset = settings.height / 2;
    settings.zoom = 300;
  }

  private applySettingsChange(event: SettingsEvent) {
    // FIXME Fix this -> maybe connect to subject or something.
    const fractalSettings = this.settingsSubject.getValue();

    switch (event.type) {
      case SettingsType.DIRECTION_X:
        fractalSettings.xOffset += event.value;
        break;
      case SettingsType.DIRECTION_Y:
        fractalSettings.yOffset += event.value;
        break;
      case SettingsType.ITERATIONS:
        fractalSettings.iterations += event.value;
        break;
      case SettingsType.ZOOM:
        fractalSettings.zoom += event.value;
        break;
      case SettingsType.RESET:
        // ToDo Implement
        break;
    }
  }
}
