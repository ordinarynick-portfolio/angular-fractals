import {Injectable} from '@angular/core';
import {BehaviorSubject, merge, Observable} from 'rxjs';
import {SettingsEvent} from '../classes/settings-event.enum';
import {KeyboardService} from './keyboard.service';
import {MouseService} from './mouse.service';

@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  // FIXME Fix behaviour to something more readonyl
  public readonly fractalInfoVisibility: BehaviorSubject<boolean>;
  public readonly fractalSettingsVisibility: BehaviorSubject<boolean>;
  public readonly fractalControlsVisibility: BehaviorSubject<boolean>;
  public readonly settingsEventStream: Observable<SettingsEvent>;

  // FIXME Mybe rethink this whole service.
  constructor(
    private keyboardService: KeyboardService,
    private mouseService: MouseService
  ) {
    this.fractalInfoVisibility = new BehaviorSubject<boolean>(true);
    this.fractalSettingsVisibility = new BehaviorSubject<boolean>(true);
    this.fractalControlsVisibility = new BehaviorSubject<boolean>(true);

    this.settingsEventStream = merge(keyboardService.getSettingEvents(), mouseService.getSettingEvents());
  }
}
