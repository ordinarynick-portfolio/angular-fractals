import { TestBed, inject } from '@angular/core/testing';

import { FractalService } from './fractal.service';

describe('FractalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FractalService]
    });
  });

  it('should be created', inject([FractalService], (service: FractalService) => {
    expect(service).toBeTruthy();
  }));
});
