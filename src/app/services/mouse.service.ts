import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {SettingsEvent} from '../classes/settings-event.enum';
import {SettingsType} from '../enums/settings-type.enum';

/**
 * Simple service which catch {@link MouseEvent} and translate them to {@link SettingsEvent}.
 */
@Injectable({
  providedIn: 'root'
})
export class MouseService {

  private mouseMove: boolean;
  private readonly settingsEventStream: BehaviorSubject<SettingsEvent> =
    new BehaviorSubject<SettingsEvent>(new SettingsEvent(SettingsType.RESET, 0));

  constructor() {
    this.registerEventListeners();
  }

  /**
   * @return Returns {@link Observable} of {@link SettingsEvent}s.
   */
  public getSettingEvents(): Observable<SettingsEvent> {
    return this.settingsEventStream;
  }

  private registerEventListeners(): void {
    window.addEventListener('mousedown', (event) => this.onMouseDown(event));
    window.addEventListener('mouseup', (event) => this.onMouseUp(event));
    window.addEventListener('mousemove', (event) => this.onMouseMove(event));
    window.addEventListener('mousewheel', (event) => this.onMouseWheel(event));
  }

  private onMouseDown(event: MouseEvent) {
    this.mouseMove = true;
  }

  private onMouseUp(event: MouseEvent) {
    this.mouseMove = false;
  }

  private onMouseMove(event: MouseEvent) {
    if (this.mouseMove) {
      this.settingsEventStream.next(new SettingsEvent(SettingsType.DIRECTION_X, event.movementX));
      this.settingsEventStream.next(new SettingsEvent(SettingsType.DIRECTION_Y, -event.movementY));
    }
  }

  private onMouseWheel(event: WheelEvent) {
    this.settingsEventStream.next(new SettingsEvent(SettingsType.ZOOM, event.wheelDelta));
  }
}
