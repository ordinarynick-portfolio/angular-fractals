import {Component, OnInit} from '@angular/core';
import {IFractalSettings} from './interfaces/fractal-settings';
import {IFractal} from './interfaces/fractal';
import {FractalService} from './services/fractal.service';
import {InteractionService} from './services/interaction.service';
import {interval} from 'rxjs';
import {debounce} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  fractalSettings: IFractalSettings;
  fractal: IFractal<IFractalSettings>;
  canvas: HTMLCanvasElement;

  // FIXME Separate info to componen
  // FIXME Factory - interface for creating fractal engines
  // FIXME Factory - will also create settings for that
  // FIXME I must somehow push height and width to fractal at begin.
  constructor(
    private fractalService: FractalService,
    private interactionService: InteractionService,
  ) {
    this.registerSubscribers();
  }

  private registerSubscribers() {
    this.fractalService.getFractalObservable().subscribe((fractal) => {
      this.fractal = fractal;
      this.render();
    });

    this.fractalService.getFractalSettingsObservable().subscribe((settings) => {
      this.fractalSettings = settings;
      this.render();
    });

    // FIXME Maybe move somewhere
    this.interactionService.settingsEventStream
      .pipe(debounce(() => interval(10)))
      .subscribe(() => this.render());
  }

  ngOnInit() {
    this.canvas = this.createCanvas();
    this.placeCanvas(this.canvas);
  }

  render(): void {
    if (this.fractalSettings && this.canvas) {
      this.fractal.render(this.canvas, this.fractalSettings);
    }
  }

  createCanvas(): HTMLCanvasElement {
    const canvas = <HTMLCanvasElement>document.getElementById('canvas');

    //const canvas = new HTMLCanvasElement();

    canvas.id = 'canvas';
    canvas.height = innerHeight;
    canvas.width = innerWidth;

    return canvas;
  }

  placeCanvas(canvas: HTMLCanvasElement): void {
    const placeForCanvas = document.getElementById('canvasDiv');
    while (placeForCanvas.firstChild) {
      placeForCanvas.removeChild(placeForCanvas.firstChild);
    }
    placeForCanvas.appendChild(canvas);
  }
}
